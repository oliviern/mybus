Keychain Access
---------------
In the Certificate Information window, enter the following information:
In the User Email Address field, enter your email address.
In the Common Name field, create a name for your private key (e.g., John Doe Dev Key).
The CA Email Address field should be left empty.
In the "Request is" group, select the "Saved to disk" option.

Certificate distribution
------------------------
https://developer.apple.com/account/ios/certificate/certificateList.action?type=distribution
upload CertificateSigningRequest.certSigningRequest
download ios_distribution.cer
open ios_distribution.cer
visible in keychain/certificates (with Common Name field)

Distribution provisioning profile
---------------------------------
https://developer.apple.com/account/ios/profile/production
Distribution/AppStore
appId : io.thebus.myBUS
Select certificate (ios_distribution.cer)
Download : mybus_distribution_provisioning_profile.mobileprovision
Open it : added via/to Xcode

visible in cd ~/Library/MobileDevice/Provisioning\ Profiles 

Compilation
-----------

➜  mybus git:(master) ✗ tns run ios --provision
Searching for devices...
┌───────────────────────────────────────────┬───────────────┬──────────────┬─────────┐
│ Provision Name / Provision UUID / App Id  │ Team          │ Type / Due   │ Devices │
│                                           │               │              │         │
│ 'mybus distribution provisioning profile' │ olivier nerot │ Distribution │         │
│ 793f1c01-2c6b-4133-b4df-6ad9e66a6e0e      │ (8VSZ3GJF4Y)  │ 3 Feb 2020   │         │
│ 8VSZ3GJF4Y.io.thebus.myBUS                │               │              │         │
└───────────────────────────────────────────┴───────────────┴──────────────┴─────────┘

There are also 0 non-eligable provisioning profiles.

➜  mybus git:(master) ✗ tns build ios --provision 793f1c01-2c6b-4133-b4df-6ad9e66a6e0e --release --for-device

(popup to authorize key access... 4x)
...

The build result is located at: /Users/olivier/Devs/mybus/platforms/ios/build/device/mybus.ipa


Upload
------
XCode / Application Loader

visible dans : https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1448699242/testflight?section=iosbuilds

