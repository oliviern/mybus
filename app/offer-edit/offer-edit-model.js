const observableModule = require("tns-core-modules/data/observable");
const application = require("tns-core-modules/application");
const firebase = require("nativescript-plugin-firebase");

function OfferEditModel() {

    const viewModel = observableModule.fromObject({
        id:0,
        title:'',
        content:'',
        price:'',
        location: firebase.firestore.GeoPoint(0,0),
        image: (application.ios)?"res://plus":"res://plus",
        // picture1: (application.ios)?"res://plus":"res://plus",
        // picture2: (application.ios)?"res://plus":"res://plus",
        // picture3: (application.ios)?"res://plus":"res://plus",
        // picture4: (application.ios)?"res://plus":"res://plus",
        active:false,
        followme:['never','1 hour','6 hours','1 day','allways'],
      }
    );

    return viewModel;
}

module.exports = OfferEditModel;
