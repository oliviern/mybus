const app = require("tns-core-modules/application");
const platformModule = require("platform");
const observableModule = require("tns-core-modules/data/observable");
const frameModule = require("tns-core-modules/ui/frame");
const camera = require("nativescript-camera");
const imageModule = require("tns-core-modules/ui/image");
const imageSourceModule = require("tns-core-modules/image-source");
const OfferEditModel = require("./offer-edit-model");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
const firebase = require("nativescript-plugin-firebase");
var FeedbackPlugin = require("nativescript-feedback");
var feedback = new FeedbackPlugin.Feedback();
const dialogs = require("tns-core-modules/ui/dialogs");
const imagepicker = require("nativescript-imagepicker");

const fs = require("tns-core-modules/file-system");
const http = require("http");
var color = require("color");

var base64img = [];

function onNavigatingTo(args) {
    const page = args.object;

    if (args.isBackNavigation) return;

    page.bindingContext = (args.context)?observableModule.fromObject(args.context):new OfferEditModel();
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

var newimage = false;

function onAddImage(args) {
  newimage = false;

  function saveImage(imageAsset) {
    args.object.page.bindingContext.set('image',imageAsset);
    const source = new imageSourceModule.ImageSource();
    source.fromAsset(imageAsset).then(function(is){
      const folder = fs.knownFolders.documents().path;
      const path = fs.path.join(folder, "offer.png");
      is.saveToFile(path, "png");
    });
  }

  dialogs.action({
      message: "Take picture",
      cancelButtonText: "CANCEL",
      actions: ["FROM CAMERA", "FROM GALLERY"]
  }).then(function (result) {

      if(result == "FROM CAMERA"){

        camera.requestPermissions().then(
        function success() {
          camera.takePicture({
                                  height: 200,
                                  width: 200,
                                  keepAspectRatio: true,
                                  saveToGallery: false
                              })
            .then(function (imageAsset) {
              saveImage(imageAsset);
              newimage=true;

            }).catch(function (err) {
                console.log("Error -> " + err.message);
            });
        },
        function failure() {
        }
        );

      }else if(result == "FROM GALLERY"){
          //Do action2
          var context = imagepicker.create({ mode: "single" });

          context
            .authorize()
            .then(function() {
                return context.present();
            })
            .then(function(selection) {
                selection.forEach(function(selected) {
                    // process the selected image
                    saveImage(selected);
                });
                newimage=true;
            }).catch(function (e) {
                // process error
            });
      }
  });

  //const id = args.object.id;

}

function uploadImage(id) {
  if (newimage==false) {
    frameModule.topmost().goBack();
    return;
  }

  const folder = fs.knownFolders.documents().path;
  const path = fs.path.join(folder, "offer.png");
  firebase.storage.uploadFile({
    remoteFullPath: 'offers/offer'+id+'.png',
    localFullPath: path
  }).then(
    function (uploadedFile) {
      firebase.storage.getDownloadUrl({
        remoteFullPath: 'offers/offer'+id+'.png'
      }).then(
          function (url) {
            console.log("Remote URL: " + url);
            var offer = global.offers_firebase.doc(id);
            offer.update({image:url}).then(function(){
              frameModule.topmost().goBack();
            })
          },
          function (error) {
            console.log("Error: " + error);
          }
      );
    },
    function (error) {
      console.log("File upload error: " + error);
    }
  );

}

function onTapSave(args) {
  var bc = args.object.page.bindingContext;

  if (bc.id==0) // NEW OFFER
    global.offers_firebase.add({
      title : bc.get('title'),
      content: bc.get('content'),
      price: bc.get('price'),
      owner_id: userService.getId(),
      location: bc.location,
      image: 'https://firebasestorage.googleapis.com/v0/b/mybus-97f0f.appspot.com/o/logo%20simple.png?alt=media&token=3be2c2e6-5471-4afd-a85d-4523feb16b97',
      active: bc.get('active'),
    }).then(function(result) {

        uploadImage(result.id);

        feedback.success({
          title: "OFFER CREATED",
          titleColor: new color.Color("#F07000"),
          message: bc.get('title')+" is created as your new offer",
          messageColor: new color.Color("#FFFFFF"),
          duration: 5000,
          //backgroundColor: new color.Color("#000000"),
        });

        if (bc.get('active'))
          global.pushNotification(
            '/topics/pushok',
            'NEW OFFER',
            userService.getName()+' has created a new offer : '+bc.get('title'),
            { 'page':'offer/offer-page',
              'type':'offer',
              'id':result.id
            }
          );

        //frameModule.topmost().goBack();
        //args.object.page.frame.goBack();

    }, function(error) {
        console.error(error);
        feedback.error({
          title: "ERROR",
          message: error
        });
    });

    if (bc.id!=0) {// OLD OFFERS
      var offer = global.offers_firebase.doc(bc.id);
      offer.update({
        title : bc.title,
        content: bc.content,
        price: bc.price,
        location: bc.location,
        //image: 'https://firebasestorage.googleapis.com/v0/b/mybus-97f0f.appspot.com/o/logo%20simple.png?alt=media&token=3be2c2e6-5471-4afd-a85d-4523feb16b97',
        active: bc.active,
      }).then(() => {

        uploadImage(bc.id);

        feedback.success({
          title: "OFFER UPDATED",
          titleColor: new color.Color("#F07000"),
          message: bc.title+" is updated",
          messageColor: new color.Color("#FFFFFF"),
          duration: 5000,
          //backgroundColor: new color.Color("#000000"),
        });

        if (bc.active)
          global.pushNotification(
            '/topics/pushok',
            'OFFER EDITED',
            userService.getName()+' has edited the offer : '+bc.get('title'),
            { 'page':'offer/offer-page',
              'type':'offer',
              'id':bc.id
            }
          );

        //frameModule.topmost().goBack();
      }).catch((err)=> {
        console.log(err);
      })

    }
}

function onTapLocation(args) {
  frameModule.topmost().navigate({
      moduleName: "offer-location/offer-location-page",
      context : args.object.page.bindingContext,
      transition: {
          name: "slideLeft",
          duration: 1000,
      }
  });
}

exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
exports.onAddImage = onAddImage;
exports.onTapSave = onTapSave;
exports.onTapLocation = onTapLocation;
