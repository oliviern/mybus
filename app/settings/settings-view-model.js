const observableModule = require("tns-core-modules/data/observable");
const application = require("tns-core-modules/application");

const SelectedPageService = require("../shared/selected-page-service");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
// const ThanksService = require("../shared/thanks-service");

function SettingsViewModel() {
    SelectedPageService.getInstance().updateSelectedPage("Settings");

    const viewModel = observableModule.fromObject({
        user : userService.getUser(),
        thanks : thanksService.getThanks(),
    });

    return viewModel;
}

module.exports = SettingsViewModel;
