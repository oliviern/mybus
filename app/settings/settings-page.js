const app = require("tns-core-modules/application");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
const ThanksService = require("../shared/thanks-service");
const thanksService = ThanksService.getInstance();
const BarcodeScanner = require("nativescript-barcodescanner").BarcodeScanner;
const barcodescanner = new BarcodeScanner();
const camera = require("nativescript-camera");
const imageSourceModule = require("tns-core-modules/image-source");
const FeedbackPlugin = require("nativescript-feedback");
const feedback = new FeedbackPlugin.Feedback();
const dialogs = require("tns-core-modules/ui/dialogs");
const imagepicker = require("nativescript-imagepicker");

const firebase = require("nativescript-plugin-firebase");
const fs = require("tns-core-modules/file-system");

const SettingsViewModel = require("./settings-view-model");


var base64img;

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = new SettingsViewModel();
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

// function onTapScan(args) {
//
//   barcodescanner.scan({
//       formats: "QR_CODE",   // Pass in of you want to restrict scanning to certain types
//       cancelLabel: "SCAN QRCODE or CLICK HERE TO EXIT", // iOS only, default 'Close'
//       cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
//       //message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
//       //showFlipCameraButton: true,   // default false
//       preferFrontCamera: false,     // default false
//       showTorchButton: true,        // default false
//       beepOnScan: true,             // Play or Suppress beep on scan (default true)
//       torchOn: false,               // launch with the flashlight on (default false)
//       closeCallback: function () { console.log("Scanner closed"); }, // invoked when the scanner was closed (success or abort)
//       resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
//       orientation: "portrait",     // Android only, optionally lock the orientation to either "portrait" or "landscape"
//       openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
//     }).then(
//         function(result) {
//           const page = args.object.parent;
//           page.getViewById('idprivatekey').text=result.text;
//         },
//         function(error) {
//           console.log("No scan: " + error);
//         }
//     );
// }

function onAddImage(args) {

  function saveImage(imageAsset) {

    const source = new imageSourceModule.ImageSource();
    source.fromAsset(imageAsset).then(function(is){
      //base64img = is.toBase64String("png");
      const folder = fs.knownFolders.documents().path;
      const fileName = "profile.png";
      const path = fs.path.join(folder, fileName);
      if (is.saveToFile(path, "png")) {
        console.log("file saved");
        firebase.storage.uploadFile({
          remoteFullPath: 'users/profile'+userService.getId()+'.png',
          localFullPath: path,
          onProgress: function(status) {
            console.log("Uploaded fraction: " + status.fractionCompleted);
            console.log("Percentage complete: " + status.percentageCompleted);
          }
        }).then(
          function (uploadedFile) {
            console.log("File uploaded: " + JSON.stringify(uploadedFile));
            firebase.storage.getDownloadUrl({
              remoteFullPath: 'users/profile'+userService.getId()+'.png'
            }).then(
                function (url) {
                  console.log("Remote URL: " + url);
                  userService.setImage(url);
                  global.users_firebase.doc(userService.getId()).update({
                    image: url
                  })
                },
                function (error) {
                  console.log("Error: " + error);
                }
            );
          },
          function (error) {
            console.log("File upload error: " + error);
          }
        );
      }
    });

  }

  dialogs.action({
      message: "Take picture",
      cancelButtonText: "CANCEL",
      actions: ["FROM CAMERA", "FROM GALLERY"]
  }).then(function (result) {

      if(result == "FROM CAMERA"){

        camera.requestPermissions().then(
        function success() {
          camera.takePicture({
                                  height: 200,
                                  width: 200,
                                  keepAspectRatio: true,
                                  saveToGallery: false
                              })
            .then(function (imageAsset) {
              saveImage(imageAsset);

            }).catch(function (err) {
                console.log("Error -> " + err.message);
            });
        },
        function failure() {
        }
        );

      }else if(result == "FROM GALLERY"){
          //Do action2
          var context = imagepicker.create({ mode: "single" });

          context
            .authorize()
            .then(function() {
                return context.present();
            })
            .then(function(selection) {
                selection.forEach(function(selected) {
                    // process the selected image
                    saveImage(selected);
                });
                list.items = selection;
            }).catch(function (e) {
                // process error
            });
      }
  });

}

function onPrivateKey(args) {
  args.object.page.frame.navigate({
    moduleName: "./privatekey/privatekey-page",
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn"
    }
  });
}

function onSave(args) {
  const nuser = args.object.page.bindingContext.get('user')

  const user = userService.getUser();
  global.users_firebase.doc(user.id).update({
    pseudo: nuser.pseudo,
    firstname: nuser.firstname,
    lastname: nuser.lastname,
    email: nuser.email,
    presentation : nuser.presentation,
  }).then(function() {
    feedback.success({
      title: "PROFILE SAVED",
      message: "Your profile has been updated"
    });
  })
}

exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
exports.onAddImage = onAddImage;
exports.onPrivateKey = onPrivateKey;
exports.onSave = onSave;
