const observableModule = require("tns-core-modules/data/observable");
const SelectedPageService = require("../shared/selected-page-service");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
const ThanksService = require("../shared/thanks-service");

// var web3jsraw = require("web3js-raw");
require("nativescript-nodeify");
const Eth = require('ethjs');

//let Exonum = require('exonum-client')

function ThanksViewModel() {
    SelectedPageService.getInstance().updateSelectedPage("Thanks");

    const viewModel = observableModule.fromObject({
        nbthx: '',
        thanks: global.thanksService.getThanks(),
        user: global.userService.getUser(),
        mypk: global.userService.getPublickey().toLowerCase(),
        favorites: global.users?global.users.filter(user => userService.isFavorite(user.id)):[],
        nearest: global.users?global.users.filter(user => user.dist&&(user.dist<1000) ):[],

        visibilityAddress:true,
        visibilityEmail:false,
        visibilityNearest:false,
        visibilityFavourites:false,

        toaddress: '',
        toname: '',

        viewaddr:false,
        viewqrcode:true,
    });

    return viewModel;
}

module.exports = ThanksViewModel;
