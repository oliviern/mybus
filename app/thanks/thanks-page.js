const app = require("tns-core-modules/application");
const frameModule = require("tns-core-modules/ui/frame");
const Color = require("tns-core-modules/color").Color;
var FeedbackPlugin = require("nativescript-feedback");
var feedback = new FeedbackPlugin.Feedback();
const NumericKeyboard =require("nativescript-numeric-keyboard").NumericKeyboard;
const ZXing = require('nativescript-zxing');
const BarcodeScanner = require("nativescript-barcodescanner").BarcodeScanner;
const barcodescanner = new BarcodeScanner();
const clipboard = require("nativescript-clipboard");

const ThanksViewModel = require("./thanks-view-model");

const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
const user = userService.getUser();

const ThanksService = require("../shared/thanks-service");
const thanksService = ThanksService.getInstance();

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = new ThanksViewModel();

    if (args.context) {
      page.bindingContext.set('toaddress',args.context.toaddress);
      page.bindingContext.set('toname',args.context.toname);
    }
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

function onLoaded(args) {
  var page = args.object;

  var imageSource = require('image-source');
  var zx = new ZXing();

	img = page.getViewById('imgthx');
  img.imageSource = imageSource.fromNativeSource(
    zx.createBarcode({
      encode: user.get('publickey'),
      height: 400,
      width: 400,
      format: ZXing.QR_CODE})
  );

  // BUG clavier numerique iphone ... vm.get('nbthx')=='';
  // new NumericKeyboard().decorate({
  //   textField: page.getViewById("idthx"),
  //   returnKeyTitle: "THX",
  //   locale: "en_US", // or "nl_NL", or any valid locale really (to define the decimal char)
  //   noDecimals: true,
  //   noIpadInputBar: true, // suppress the bar with buttons iOS renders on iPad since iOS 9
  //   returnKeyButtonBackgroundColor: new Color("#F07000") // optional, set this to change the (default) blue color of the 'return' key
  // });
}

function sbLoaded(args) {
    // handle selected index change
    const segmentedBarComponent = args.object;
    segmentedBarComponent.on("selectedIndexChange", (sbargs) => {
        const page = sbargs.object.page;
        const vm = page.bindingContext;
        const selectedIndex = sbargs.object.selectedIndex;
        //vm.set("prop", selectedIndex);
        switch (selectedIndex) {
            case 0:
                vm.set("visibilityAddress", true);
                vm.set("visibilityEmail", false);
                vm.set("visibilityNearest", false);
                vm.set("visibilityFavourites", false);
                break;
            case 1:
                vm.set("visibilityAddress", false);
                vm.set("visibilityEmail", true);
                vm.set("visibilityNearest", false);
                vm.set("visibilityFavourites", false);
                break;
            case 2:
                vm.set("visibilityAddress", false);
                vm.set("visibilityEmail", false);
                vm.set("visibilityNearest", false);
                vm.set("visibilityFavourites", true);
                break;
            case 3:
                vm.set("visibilityAddress", false);
                vm.set("visibilityEmail", false);
                vm.set("visibilityNearest", true);
                vm.set("visibilityFavourites", false);
                break;
            default:
                break;
        }
    });
}

function sbGetLoaded(args) {
    // handle selected index change
    const segmentedBarComponent = args.object;
    segmentedBarComponent.on("selectedIndexChange", (sbargs) => {
        const page = sbargs.object.page;
        const vm = page.bindingContext;
        const selectedIndex = sbargs.object.selectedIndex;
        //vm.set("prop", selectedIndex);
        switch (selectedIndex) {
            case 1:
                vm.set("viewaddr", true);
                vm.set("viewqrcode", false);
                break;
            case 0:
                vm.set("viewaddr", false);
                vm.set("viewqrcode", true);
                break;
            default:
                break;
        }
    });
}
function onTapScan(args) {

  // barcodescanner.hasCameraPermission().then(
  //   function(granted) {
  //     // if this is 'false' you probably want to call 'requestCameraPermission' now
  //     if (granted==false)
  //       barcodescanner.requestCameraPermission().then(
  //         function() {
  //           console.log("Camera permission requested");
  //         }
  //       );
  //     console.log("Has Camera Permission? " + result);
  //   }
  // );

  barcodescanner.scan({
      formats: "QR_CODE",   // Pass in of you want to restrict scanning to certain types
      cancelLabel: "SCAN QRCODE or CLICK HERE TO EXIT", // iOS only, default 'Close'
      cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
      //message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
      //showFlipCameraButton: true,   // default false
      preferFrontCamera: false,     // default false
      showTorchButton: true,        // default false
      beepOnScan: true,             // Play or Suppress beep on scan (default true)
      torchOn: false,               // launch with the flashlight on (default false)
      closeCallback: function () { console.log("Scanner closed"); }, // invoked when the scanner was closed (success or abort)
      resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
      orientation: "portrait",     // Android only, optionally lock the orientation to either "portrait" or "landscape"
      openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
    }).then(
        function(result) {
          console.log("Scan format: " + result.format);
          console.log("Scan text:   " + result.text);

          const page = args.object.parent;
          page.getViewById('idaddress').text=result.text;
        },
        function(error) {
          console.log("No scan: " + error);
        }
    );
}

function onTapAirdrop(args) {
  // thanksService.airdropThx("0x65045a4b9d9e7cb5d051835514cab52fbe80b8ac");
  // // 0xb4149c0e3d589c4bba7ae90f0ba0c0d4b33d284a
  // return;

  barcodescanner.scan({
      formats: "QR_CODE",   // Pass in of you want to restrict scanning to certain types
      cancelLabel: "SCAN QRCODE or CLICK HERE TO EXIT", // iOS only, default 'Close'
      cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
      //message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
      //showFlipCameraButton: true,   // default false
      preferFrontCamera: false,     // default false
      showTorchButton: true,        // default false
      beepOnScan: true,             // Play or Suppress beep on scan (default true)
      torchOn: false,               // launch with the flashlight on (default false)
      closeCallback: function () { console.log("Scanner closed"); }, // invoked when the scanner was closed (success or abort)
      resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
      orientation: "landscape",     // Android only, optionally lock the orientation to either "portrait" or "landscape"
      openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
    }).then(
        function(result) {
          console.log("Scan format: " + result.format);
          console.log("Scan text:   " + result.text);

          var airdrop = result.text.replace("http://thebus.io/devenir-b-upper/","");

          thanksService.airdropThx(airdrop);
        },
        function(error) {
          console.log("No scan: " + error);
        }
    );
}

function onTapSend(args) {
  const page = args.object.page;
  const vm = page.bindingContext;

  console.log("nbthx"+vm.get('nbthx'));

  var amount = parseInt(vm.get('nbthx'));
  // BUG clavier numerique iphone ... vm.get('nbthx');
  var address = '';
  var name = '';

  // TO ADDRESS
  if (vm.get('visibilityAddress')==true) {
    address = page.getViewById('idaddress').text;
    name = (vm.toname==undefined)?'':vm.toname;
    console.log("SEND TO ADDRESS : "+address);
  }
  // TO EMAIL
  if (vm.get('visibilityEmail')==true) {
    // address = '';
    // name = page.getViewById('idemail').text;
    console.log("SEND TO EMAIL : "+address);
  }
  // TO FAVORITE
  if (vm.get('visibilityFavourites')==true) {
    // address = page.getViewById('idfavorite').selectedValue;
    // name = userService.getName(global.users._array[page.getViewById('idfavorite').selectedIndex]);
    console.log("SEND TO FAVORITE : "+address);
  }
  // TO NEAREST
  if (vm.get('visibilityNearest')==true) {
    address = page.getViewById('idnearest').selectedValue;
    console.log("SEND TO NEAREST : "+address);
  }

  if ((amount==0)||(amount==undefined)) {
    feedback.error({
      title: "BAD AMOUNT",
      message: "You can not send 0 THX"
    });
    return;
  }

  if (amount>parseInt(user.get('thx'))) {
    feedback.error({
      title: "BAD AMOUNT",
      message: "You don't have enough THX"
    });
    return;
  }

  if (address==user.get('publickey')) {
    feedback.error({
      title: "BAD ADDRESS",
      message: "You can not send THX to yourself"
    });
    return;
  }

  if (address=='') {
    feedback.error({
      title: "NO RECIPIENT",
      message: "You have to specify a recipient's address"
    });
    return;
  }

  // thanksService.transaction.amount = amount;
  // thanksService.transaction.toaddress = address;
  // thanksService.transaction.toname = name;

  //page.getViewById('idconfirmtext').text="Your are sending "+amount+" THX to "+address+"\nAre you sure ?";

  // var fpp = page.getViewById('idconfirm');
  // fpp.className = 'showit';
  frameModule.topmost().navigate({
      moduleName: "thanksconfirm/thanksconfirm-page",
      context: {
        amount: amount,
        toaddress: address,
        toname: name,
        // offerid: 0,
        // offername: '',
      },
      transition: {
          name: "slideBottom",
          duration: 1000,
      }
  });
}

function onTapPublicKey(args) {
  clipboard.setText(user.get('publickey')).then(function() {
    feedback.success({
      title: "PUBLIC KEY COPIED",
      message: "Your public key is copied to clipboard"
    });
  })
}

exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
exports.onLoaded = onLoaded;
exports.sbLoaded = sbLoaded;
exports.sbGetLoaded = sbGetLoaded;
exports.onTapSend = onTapSend;
exports.onTapScan = onTapScan;
exports.onTapAirdrop = onTapAirdrop;
exports.onTapPublicKey = onTapPublicKey
