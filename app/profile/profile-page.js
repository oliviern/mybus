const app = require("tns-core-modules/application");
const platformModule = require("platform");
const frameModule = require("tns-core-modules/ui/frame");
const utilsModule = require("tns-core-modules/utils/utils");
const ProfileViewModel = require("./profile-view-model");
const Color = require("tns-core-modules/color").Color;

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = new ProfileViewModel();
}


function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

function onAddOffer(args) {
  args.object.page.frame.navigate({
    moduleName: "./offer-edit/offer-edit-page",
    animated: true,
    transition: {
      name: "slideTop",
      duration: 380,
      curve: "easeIn"
    }
  });
}

function onAddPlace(args) {
  args.object.page.frame.navigate({
    moduleName: "./place-edit/place-edit-page",
    animated: true,
    transition: {
      name: "slideTop",
      duration: 380,
      curve: "easeIn"
    }
  });
}

function onOpen(args) {
  const item = args.object.items.getItem(args.index);
  // args.object.getDataItem(args.index);
  args.object.page.frame.navigate({
    moduleName: "./offer/offer-page",
    context : item,
    // animated: true,
    // transition: {
    //   name: "flipLeft",
    //   duration: 380,
    //   curve: "easeIn"
    // }
  });
}

function onItemLoading(args) {
    // hack to get around issue with RadListView ios background colors: https://github.com/telerik/nativescript-ui-feedback/issues/196
    if (platformModule.isIOS) {
        var newcolor = new Color("#000000");
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

function onTapPrivatekey(args) {
  frameModule.topmost().navigate({
      moduleName: "settings/settings-page",
      transition: {
          name: "fade"
      }
  });
}

function openbug(args) {
  utilsModule.openUrl("https://bitbucket.org/oliviern/mybus");
}

function deleteNotif(args) {
  const page = args.object.page;

    var id = parseInt(args.object.parent.id);

    var ns = page.bindingContext.get('notifications');
    var i = ns._array.findIndex(function(n){return(n.id==id)});
    ns.splice(i,1);
    //page.bindingContext.set('notifications',ns);
}


exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
exports.onAddOffer = onAddOffer;
exports.onAddPlace = onAddPlace;
exports.onItemLoading = onItemLoading;
exports.onOpen = onOpen;
exports.onTapPrivatekey = onTapPrivatekey;
exports.openbug = openbug;
exports.deleteNotif = deleteNotif
