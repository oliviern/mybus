const observableModule = require("tns-core-modules/data/observable");
const UserService = require("../shared/user-service");
const SelectedPageService = require("../shared/selected-page-service");
const ObservableArray = require("tns-core-modules/data/observable-array").ObservableArray;

function ProfileViewModel() {
    SelectedPageService.getInstance().updateSelectedPage("Profile");

    // var myitems = [];
    // const user = global.userService.getUser();
    // const pk = user.get('publickey');
    // console.log("mol="+global.myoffers._array.length);

    const viewModel = observableModule.fromObject({
      //visdesc: (global.myoffers._array.length==0),
      myoffers: global.myoffers,
      user: global.userService.getUser(),
      places : global.places,
      myuid: global.userService.getId(),
      notifications : new ObservableArray(global.userService.getNotifications()),
    });

    // if (pk)
    // {
    //   global.getItems(pk).then(function(result) {
    //     viewModel.set('items',result);
    //   })
    //
    //   global.getPlaces(pk).then(function(result) {
    //     viewModel.set('places',result);
    //   })
    // }


    return viewModel;
}

module.exports = ProfileViewModel;
