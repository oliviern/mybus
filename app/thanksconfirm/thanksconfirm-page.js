const app = require("tns-core-modules/application");
const frameModule = require("tns-core-modules/ui/frame");
const ThanksService = require("../shared/thanks-service");
const thanksService = ThanksService.getInstance();
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
const FeedbackPlugin = require("nativescript-feedback");
const feedback = new FeedbackPlugin.Feedback();

var amount;
var toname;
var toaddress;
var offerid;
var offername;

var code ='';

function onNavigatingTo(args) {
  var page = args.object;

  code='';

  amount = args.context.amount; //thanksService.transaction.amount;
  toname = args.context.toname; //thanksService.transaction.toname;
  toaddress = args.context.toaddress; //thanksService.transaction.toaddress;
  offerid = args.context.offerid;
  offername = args.context.offername;

  page.getViewById('idconfirmtext').text="SEND "+amount+" THX to "+toname+" ?\n ("+toaddress+")\n";

}

function onConfirm(args) {
  const page = args.object.page;

  const options = userService.getOptions();

  if (options.get('codeRequired')==true)
  if (code!=userService.getCode())
  {
    feedback.error({
      title: "BAD CODE",
      message: "You have not entered a valid code"
    });
    return;
  }
  // const amount = thanksService.transaction.amount;
  // const toname = thanksService.transaction.toname;
  // const toaddress = thanksService.transaction.toaddress;
  var message = page.getViewById('idmessage').text

  thanksService.sendThx(toaddress,amount,offerid,message);

  amount = 0;
  toname ='';
  toaddress ='';

  // frameModule.topmost().navigate({
  //     moduleName: "thanks/thanks-page",
  //     transition: {
  //         name: "slideTop",
  //         duration: 1000,
  //     }
  // });

  frameModule.topmost().goBack();

}

function onTapCode(args) {
  const page = args.object.page;
  const c = args.object.text;

  if (c=='<<')
    code = code.slice(0, -1);
  else
    code += c;

  console.log(code);
  page.getViewById('idcode').text = code.replace(/./gi, '*');
}


exports.onNavigatingTo = onNavigatingTo;
exports.onConfirm = onConfirm;
exports.onTapCode = onTapCode;
