const app = require("tns-core-modules/application");
const platformModule = require("platform");
const frameModule = require("tns-core-modules/ui/frame");
const firebase = require("nativescript-plugin-firebase");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
const Color = require("tns-core-modules/color").Color;
const observableModule = require("tns-core-modules/data/observable");
const clipboard = require("nativescript-clipboard");
//const ItemViewModel = require("./item-view-model");

function onNavigatingTo(args) {
    const page = args.object;

    page.bindingContext = observableModule.fromObject({
      user : args.context,
      offers : global.getOffersByOwnerId(args.context.id),
      isFavorite : userService.isFavorite(args.context.id)
    })
}

function onItemLoading(args) {
    // hack to get around issue with RadListView ios background colors: https://github.com/telerik/nativescript-ui-feedback/issues/196
    if (platformModule.isIOS) {
        var newcolor = new Color("#303030");
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

function onOpenOffer(args) {
  const item = args.object.items[args.index];
  args.object.page.frame.navigate({
    moduleName: "./offer/offer-page",
    context: {
      id: item.id,
      owner_id: item.owner_id
    },
  });
}

function onTapPublicKey(args) {
  clipboard.setText(args.object.text).then(function() {
    feedback.success({
      title: "PUBLIC KEY COPIED",
      message: "The users's public key is copied to clipboard"
    });
  })
}

function onTapSend(args) {

  var user = args.object.bindingContext.get('user');

  frameModule.topmost().navigate({
      moduleName: "thanks/thanks-page",
      context: {
        amount: 0,
        toaddress: user.publickey,
        toname: user.name,
      },
      transition: {
          name: "slideTop",
          duration: 1000,
      }
  });

}

function onCheck(args) {
  var f = userService.user.get('favorites');
  var i = f.findIndex( function(id){return (id==args.object.id)} )

  if (args.object.checked==false) { //previous state...
    f.push(args.object.id);
  } else {
    f.splice(i,1);
  }

}

exports.onNavigatingTo = onNavigatingTo;
exports.onOpenOffer = onOpenOffer;
exports.onItemLoading = onItemLoading;
exports.onTapPublicKey = onTapPublicKey;
exports.onTapSend = onTapSend;
exports.onCheck= onCheck;
