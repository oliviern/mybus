const app = require("tns-core-modules/application");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
const ThanksService = require("../shared/thanks-service");
const thanksService = ThanksService.getInstance();
const ZXing = require('nativescript-zxing');
const imageSource = require('image-source');
const BarcodeScanner = require("nativescript-barcodescanner").BarcodeScanner;
const barcodescanner = new BarcodeScanner();
const frameModule = require("tns-core-modules/ui/frame");
const FeedbackPlugin = require("nativescript-feedback");
const feedback = new FeedbackPlugin.Feedback();

const PrivatekeyViewModel = require("./privatekey-view-model");

function createQRcode(page,pk) {
  const zx = new ZXing();

  img = page.getViewById('imgthx');
  img.imageSource = imageSource.fromNativeSource(
    zx.createBarcode({
      encode: pk,
      height: 400,
      width: 400,
      format: ZXing.QR_CODE})
  );
}

function onLoaded(args) {
  const page = args.object;
  page.bindingContext = new PrivatekeyViewModel();

  const user = userService.getUser();
  createQRcode(args.object,user.get('privatekey'));

  let pk = args.object.page.getViewById("idprivatekey");
  pk.on("textChange", function() {
    createQRcode(args.object,pk.text);
  });

}

function onTapCreatePrivatekey(args) {
  var page = args.object.page;

  if (page.bindingContext.get('locked')) {
    feedback.error({
      title: "PRIVATE KEY LOCKED",
      message: "Your private key is locked. Unlock it if you need"
    });
    return;
  }

  var user = userService.getUser();
  var pk = thanksService.randomPrivatekey()
  user.set('privatekey' , pk);

  createQRcode(args.object.page,pk);
}

function onTapScan(args) {
  var page = args.object.page;

  if (page.bindingContext.get('locked')) {
    feedback.error({
      title: "PRIVATE KEY LOCKED",
      message: "Your private key is locked. Unlock it if you need"
    });
    return;
  }

  barcodescanner.scan({
      formats: "QR_CODE",   // Pass in of you want to restrict scanning to certain types
      cancelLabel: "SCAN QRCODE or CLICK HERE TO EXIT", // iOS only, default 'Close'
      cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
      //message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
      //showFlipCameraButton: true,   // default false
      preferFrontCamera: false,     // default false
      showTorchButton: true,        // default false
      beepOnScan: true,             // Play or Suppress beep on scan (default true)
      torchOn: false,               // launch with the flashlight on (default false)
      closeCallback: function () { console.log("Scanner closed"); }, // invoked when the scanner was closed (success or abort)
      resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
      orientation: "portrait",     // Android only, optionally lock the orientation to either "portrait" or "landscape"
      openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
    }).then(
        function(result) {
          const page = args.object.parent;
          page.getViewById('idprivatekey').text=result.text;
        },
        function(error) {
          console.log("No scan: " + error);
        }
    );
}

function onExit(args) {
  var page = args.object.page;

  userService.setLockedPk(true);

  frameModule.topmost().navigate({
      moduleName: "settings/settings-page",
      transition: {
          name: "slideLeft",
          duration: 1000,
      }
  });
}

exports.onLoaded = onLoaded;
exports.onTapCreatePrivatekey = onTapCreatePrivatekey;
exports.onTapScan = onTapScan;
exports.onExit = onExit;
