const observableModule = require("tns-core-modules/data/observable");
const application = require("tns-core-modules/application");

//const SelectedPageService = require("../shared/selected-page-service");
// const UserService = require("../shared/user-service");
// const ThanksService = require("../shared/thanks-service");

function PrivatekeyViewModel() {
    //SelectedPageService.getInstance().updateSelectedPage("Settings");

    // const userService = UserService.getInstance();

    const viewModel = observableModule.fromObject({
        user : userService.getUser(),
        locked : userService.getLockedPk(),
        //thanks : thanksService.getThanks(),
    });

    return viewModel;
}

module.exports = PrivatekeyViewModel;
