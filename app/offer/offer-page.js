const app = require("tns-core-modules/application");
const platformModule = require("platform");
const frameModule = require("tns-core-modules/ui/frame");
const firebase = require("nativescript-plugin-firebase");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
//const ItemViewModel = require("./item-view-model");

var ownerdata = undefined;

function onNavigatingTo(args) {
    const page = args.object;

    ownerdata = global.getUserbyId(args.context.owner_id);

    //global.users_firebase.doc(args.context.owner_id).get().then(function(owner){
      //owner = owner;
      //var data = owner.data();

      const offer = global.offers_firebase.doc(args.context.id);
      offer.get().then(function(o){
        args.context = o.data();
        args.context.id = o.id;
        offer.collection('sold').get().then(function(sold) {
          var sdata = [];
          sold.forEach(function(s){
            var data = s.data();
            var buyer = global.getUserbyId(data.by_id);
            data.by_name= (buyer)?buyer.name:'unknown';
            sdata.push(data);
          })
          //collection.log(sold);
          args.context['sold'] = sdata;
          args.context['user'] = userService.getUser();
          args.context['owner_name'] = ownerdata.name; //(ownerdata.anonymous)?ownerdata.pseudo:ownerdata.firstname+' '+ownerdata.lastname;
          page.bindingContext = args.context;
        })
      })

    //})

}

function onEditOffer(args) {
  args.object.page.frame.navigate({
    moduleName: "./offer-edit/offer-edit-page",
    animated: true,
    context : args.object.page.bindingContext,
    transition: {
      name: "slideTop",
      duration: 380,
      curve: "easeIn"
    }
  });
}

function onTapSend(args) {
  var page = args.object.page;

  frameModule.topmost().navigate({
      moduleName: "thanksconfirm/thanksconfirm-page",
      context: {
        amount: page.bindingContext.price,
        toaddress: ownerdata.publickey,
        toname: ownerdata.name, //(ownerdata.anonymous)?data.pseudo:data.firstname+' '+data.lastname,
        offerid: page.bindingContext.id,
        offername: page.bindingContext.title,
      },
      transition: {
          name: "slideTop",
          duration: 1000,
      }
  });

}

function onTapOwner(args) {
  args.object.page.frame.navigate({
    moduleName: "./pageuser/pageuser-page",
    context: ownerdata,
  });
}

function onSoldTap(args) {
    console.log(args);
}

exports.onNavigatingTo = onNavigatingTo;
exports.onEditOffer = onEditOffer;
exports.onTapSend = onTapSend;
exports.onSoldTap = onSoldTap;
exports.onTapOwner = onTapOwner;
