const observableModule = require("tns-core-modules/data/observable");

const SelectedPageService = require("../shared/selected-page-service");

function BusViewModel() {
    SelectedPageService.getInstance().updateSelectedPage("Bus");

    const viewModel = observableModule.fromObject({
    });

    return viewModel;
}

module.exports = BusViewModel;
