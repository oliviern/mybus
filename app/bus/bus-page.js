const app = require("tns-core-modules/application");
const platformModule = require("platform");
const BusViewModel = require("./bus-view-model");
const Color = require("tns-core-modules/color").Color;

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = new BusViewModel();
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
