const app = require("tns-core-modules/application");
const platformModule = require("platform");
const frameModule = require("tns-core-modules/ui/frame");
const camera = require("nativescript-camera");
const imageModule = require("tns-core-modules/ui/image");
const PlaceEditModel = require("./place-edit-model");

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = new PlaceEditModel();
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

function onAddImage(args) {
  camera.requestPermissions().then(
  function success() {
    camera.takePicture()
      .then(function (imageAsset) {
          console.log("Result is an image asset instance");
          var image = new imageModule.Image();
          image.src = imageAsset;
      }).catch(function (err) {
          console.log("Error -> " + err.message);
      });
  },
  function failure() {
  // permission request rejected
  // ... tell the user ...
  }
  );


}

exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
exports.onAddImage = onAddImage;
