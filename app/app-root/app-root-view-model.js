const observableModule = require("tns-core-modules/data/observable");
const Observable = require("tns-core-modules/data/observable").Observable;

const SelectedPageService = require("../shared/selected-page-service");

function AppRootViewModel() {

    const viewModel = observableModule.fromObject({
        selectedPage: "",
        //mybusweb: global.mybusweb,
        user:global.userService.getUser(),
        options:global.userService.getOptions(),
        //thanks:global.thanksService.getThanks(),
        version: global.appversion,
        build: global.appbuild
    });

    SelectedPageService.getInstance().selectedPage$
    .subscribe((selectedPage) => { viewModel.selectedPage = selectedPage; });

    return viewModel;
}

module.exports = AppRootViewModel;
