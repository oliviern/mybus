const application = require("tns-core-modules/application");
const frameModule = require("tns-core-modules/ui/frame");
const appSettings = require("application-settings");
const firebase = require("nativescript-plugin-firebase");

const AppRootViewModel = require("./app-root-view-model");

function onLoaded(args) {
    const drawerComponent = args.object;
    drawerComponent.bindingContext = new AppRootViewModel();

    if (!global.userService.isLogged())
      setTimeout(() => frameModule.topmost().navigate({
          moduleName: "login/login-page",
          transition: {
              name: "fade",
              duration: 1000,
          }
      }), 0);

}

function onNavigationItemTap(args) {
    const component = args.object;
    const componentRoute = component.route;
    const componentTitle = component.title;
    const bindingContext = component.bindingContext;

    bindingContext.set("selectedPage", componentTitle);

    firebase.analytics.setScreenName({
      screenName: componentTitle
    }).then(
      function () {
        console.log("Screen name set : "+componentTitle);
      }
    );

    // firebase.analytics.logEvent({
    // key: "page_view",
    // parameters: [
    //        {
    //             key: "page_id",
    //             value: componentTitle
    //        }
    // ]
    // });

    if (!global.userService.isLogged())
      frameModule.topmost().navigate({
          moduleName: "login/login-page",
          transition: {
              name: "fade",
              duration: 1000,
          }
      });
    else
      frameModule.topmost().navigate({
          moduleName: componentRoute,
          transition: {
              name: "fade"
          }
      });

    const drawerComponent = application.getRootView();
    drawerComponent.closeDrawer();
}

function onLogout(args) {
  const drawerComponent = application.getRootView();
  drawerComponent.closeDrawer();

  appSettings.setBoolean('islogged',false);

  firebase.logout();

  frameModule.topmost().navigate({
      moduleName: "login/login-page",
  });
}

exports.onLoaded = onLoaded;
exports.onLogout = onLogout;
exports.onNavigationItemTap = onNavigationItemTap;
