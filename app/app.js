require("./bundle-config");
const application = require("tns-core-modules/application");
const appSettings = require("application-settings");
const observableModule = require("tns-core-modules/data/observable");
const firebase = require("nativescript-plugin-firebase");
const firebaseApp = require("nativescript-plugin-firebase/app");
const timerModule = require("tns-core-modules/timer");
const appversion = require("nativescript-appversion");

const ObservableArray = require("tns-core-modules/data/observable-array").ObservableArray;

const http = require("http");
const Color = require("tns-core-modules/color").Color;

const imageCache = require("nativescript-web-image-cache");
const httpModule = require("http");

application.on(application.launchEvent, (args) => {
  appversion.getVersionCode().then(function(v) {
      global.appversion = v;
  });

  appversion.getVersionName().then(function(v) {
      global.appbuild = v;
  });

    if (args.android) {
        // For Android applications, args.android is an android.content.Intent class.
        console.log("Launched Android application with the following intent: " + args.android + ".");
        imageCache.initialize();
    } else if (args.ios !== undefined) {
        // For iOS applications, args.ios is NSDictionary (launchOptions).
        console.log("Launched iOS application with options: " + args.ios);
    }
});

application.on(application.suspendEvent, (args) => {
  if (global.userService.options.autoLogout==true) {
    appSettings.setBoolean('islogged',false);
    firebase.logout();
  }

    if (args.android) {
        // For Android applications, args.android is an android activity class.
        console.log("SUSPEND Activity: " + args.android);
    } else if (args.ios) {
        // For iOS applications, args.ios is UIApplication.
        console.log("SUSPEND UIApplication: " + args.ios);
    }
});

application.on(application.exitEvent, (args) => {

    if (args.android) {
        // For Android applications, args.android is an android activity class.
        console.log("EXIT Activity: " + args.android);
    } else if (args.ios) {
        // For iOS applications, args.ios is UIApplication.
        console.log("EXIT UIApplication: " + args.ios);
    }
});

// Services
const UserService = require("./shared/user-service");
global.userService = UserService.getInstance();

const ThanksService = require("./shared/thanks-service");
global.thanksService = ThanksService.getInstance();

firebase.init({
    showNotificationsWhenInForeground: true,
}).then(function() {
  console.log("FIREBASE INIT OK")

  global.userService.init();
  global.thanksService.init();

  // firebase collections
  global.users_firebase = firebaseApp.firestore().collection("users");
  global.offers_firebase = firebaseApp.firestore().collection("offers");
  global.places_firebase = firebaseApp.firestore().collection("places");

  // firebase.getRemoteConfig({
  //   developerMode: true, // play with this boolean to get more frequent updates during development
  //   cacheExpirationSeconds: 3600, // 10 minutes, default is 12 hours.. set to a lower value during dev
  //   properties: [{
  //       key: "thankscontract"
  //     }]
  // }).then(
  //     function (result) {
  //
  //       var tc = JSON.parse(result.properties.thankscontract)
  //       console.log('address='+tc.address);
  //       console.log('node='+tc.node);
  //       global.thanksService.thanks.set('contract',tc.address);
  //       global.thanksService.thanks.set('ethNode',tc.node);
  //
  //     }
  // )

  global.places = new ObservableArray([])
  global.places_firebase.onSnapshot(function (snapshot){
    console.log('firebase places updated...')
    // global.places.length = 0;
    // snapshot.forEach(function(s) {
    //   global.places.push(s.data());
    // })
    var changes = snapshot.docChanges();
    changes.forEach(function(p) {
      var doc = p.doc;
      var data = doc.data();
      data.id = doc.id;

      switch(p.type){
        case "added":
          data.dist=undefined;
          global.places.push(observableModule.fromObject(data));
          break;
        case "modified":
          var place = global.places._array.find(function(u){return (p.id==doc.id)})
          if (place) {
            place.set('title', data.title);
            place.set('content', data.content);
            place.set('image', data.image);
            place.set('owner_uid', data.owner_uid);
            place.set('location', data.location);
          }
          break;
        case "removed":
          var index = global.places._array.findIndex(function(u){return (p.id==doc.id)});
          global.places.splice(index,1);
          break;
      }
    })
  });
  global.places_firebase.get();

  global.users = new ObservableArray([])
  global.users_firebase.onSnapshot(function (snapshot){
    console.log('firebase users updated...')
    //global.users.length = 0;
    var changes = snapshot.docChanges();
    changes.forEach(function(c) {
      var doc = c.doc;
      var data = doc.data();
      data.id = doc.id;
      //data.dist = 10000;
      data.name = (data.anonymous)? data.pseudo : data.pseudo+" ("+data.firstname+" "+data.lastname+" )";

      switch(c.type){
        case "added":
          data.dist=undefined;
          global.users.push(observableModule.fromObject(data));
          break;
        case "modified":
          var user = global.users._array.find(function(u){return (u.id==doc.id)})
          if (user) {
            user.set('anonymous', data.anonymous);
            user.set('email', data.email);
            user.set('firstname', data.firstname);
            user.set('lastname', data.lastname);
            user.set('pseudo', data.pseudo);
            user.set('presentation', data.presentation);
            user.set('publickey', data.publickey);
            user.set('location', data.location);
            user.set('visible', data.visible);
            user.set('image', data.image);
            user.set('tokenFCM', data.tokenFCM);
          }
          break;
        case "removed":
          var index = global.users._array.findIndex(function(u){return (u.id==doc.id)});
          global.users.splice(index,1);
          break;
      }
    })
    global.users.sort(function(a, b) {
      a=a.pseudo.toLowerCase();
      b=b.pseudo.toLowerCase();
      return (a==b)?0:(a>b)?1:-1;
    });
  });
  global.users_firebase.get();

  global.offers = new ObservableArray([])
  global.myoffers = new ObservableArray([])
  global.offers_firebase.onSnapshot(function (snapshot){
    console.log('firebase offers updated...')

    var changes = snapshot.docChanges();
    changes.forEach(function(c) {
      var doc = c.doc;
      var data = doc.data();
      data.id = doc.id;

      switch(c.type){
        case "added":
          data.dist = undefined;
          global.offers.push(observableModule.fromObject(data));
          break;
        case "modified":
          var offer = global.offers._array.find(function(o){return (o.id==doc.id)})
          if (offer) {
            offer.set('active', data.active);
            offer.set('title', data.title);
            offer.set('content', data.content);
            offer.set('price', data.price);
            offer.set('image', data.image);
            offer.set('location', data.location);
            offer.set('owner_id', data.owner_id);
          }
          break;
        case "removed":
          var index = global.offers._array.findIndex(function(o){return (o.id==doc.id)});
          global.offers.splice(index,1);
          break;
      }
    })
    // sort offers by dist
    global.offers.sort(function(a, b) {
      var da=a.dist?a.dist:1E18;
      var db=b.dist?b.dist:1E18;
      if (!a.active) da = 2E18;
      if (!b.active) db = 2E18;
      return (da==db)?0:(da>db)?1:-1;
    });

    // reset my offers
    global.myoffers.length = 0;
    global.offers.forEach(function(o){
      if (o.owner_id==userService.getId())
        global.myoffers.push(o);
    })

    // global.offers.length = 0;
    // global.myoffers.length = 0;
    // snapshot.forEach(function(s) {
    //   var data = s.data();
    //   data.id = s.id;
    //   //data.dist = 0;
    //   if (data.owner_id==userService.getId())
    //     global.myoffers.push(data);
    //   //else
    //   if (data.active==true)
    //   {
    //     // if ((data.location.latitude!=0)&&(data.location.longitude!=0))
    //     //   data.dist = parseInt(geolocation.distance({latitude:45.77,longitude:4.83}, data.location));
    //     global.offers.push(observableModule.fromObject(data));
    //   }
    //
    //
    // })
  });
  global.offers_firebase.get();

  // if (global.userService.options.followMe)
  //   global.userService.startLocation();

}).catch(function(err){
  console.log(err);
})

if (global.userService.options.enableNotification==true){
  firebase.addOnMessageReceivedCallback(function(message){
    userService.sendMessage(message);
  })
}

global.convertTHX = function(thx) {
  if (!thx) return '0';
  var thxs = thx.toString();
  if (thxs=='0') return '0';
  return thxs.slice(0, -18);
}


global.pushNotification = function(to,title,content,data) {

  httpModule.request({
    url: "https://fcm.googleapis.com/fcm/send",
    method: "POST",
    headers: {
      'Authorization': 'key=AAAAPPe1iKA:APA91bHMV_cGoDgDsdSmaQTRtKM9KP4faDXHm0JlA8wrvbUWA-UmN1hSS9O6OtPXwvXRPBW42HXJ6QPWQeP6FwnqdoN397M4W6g0KhPtAeBaA794L2aQ0L4HXeeMf8XAc-QYu0smfU58',
      'Content-Type': 'application/json'
    },
    content: JSON.stringify({
        "to": to, //"/topics/android",
        "data": data,
        "priority": "high",
        "notification": {
            "title": title,
            "body": content
        }
      })
    }).then(
      (response) => {
        console.log(response.content.toString());
      },
      (e) => {
        console.log(e);
      }
    );

  // $.ajax({
  //         type: 'POST',
  //         url: "https://fcm.googleapis.com/fcm/send",
  //         headers: {
  //             Authorization: 'key=mykey'
  //         },
  //         contentType: 'application/json',
  //         dataType: 'json',
  //         data: JSON.stringify({
  //             "to": "/topics/android",
  //             "priority": "high",
  //             "notification": {
  //                 "title": title,
  //                 "body": subtitle
  //             }
  //         }),
  //         success: function(responseData) {
  //             alert("Success");
  //         }
  //         error: function(jqXhr, textStatus, errorThrown) {
  //             /*alert("Fail");*/   // alerting "Fail" isn't useful in case of an error...
  //             alert("Status: " + textStatus + "\nError: " + errorThrown);
  //         }
  //     });

}

global.getUserbyId = function(id) {
  if (global.users._array==undefined) return;
  return global.users._array.find(function(user){
    return(user.id == id);
  })
}

global.getOfferbyId = function(id) {
  if (global.offers._array==undefined) return;
  return global.offers._array.find(function(offer){
    return(offer.id == id);
  })
}

global.getOffersByOwnerId = function(ownerid) {
  var offers = [];
  global.offers._array.forEach(function(offer){
    if (offer.owner_id==ownerid)
      offers.push(offer);
  })
  return offers;
}

application.run({ moduleName: "app-root/app-root" });
/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
