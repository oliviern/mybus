const app = require("tns-core-modules/application");
const platformModule = require("platform");
const frameModule = require("tns-core-modules/ui/frame");

function onTapGoPrivatekey(args) {
  var page = args.object.page;

  frameModule.topmost().navigate({
      moduleName: "privatekey/privatekey-page",
      transition: {
          name: "slideLeft",
          duration: 1000,
      }
  });

}

exports.onTapGoPrivatekey = onTapGoPrivatekey
