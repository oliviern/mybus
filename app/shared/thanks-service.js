const fromObject = require("tns-core-modules/data/observable").fromObject;
const timerModule = require("tns-core-modules/timer");
var firebase = require("nativescript-plugin-firebase");
const UserService = require("../shared/user-service");
const userService = UserService.getInstance();
var FeedbackPlugin = require("nativescript-feedback");
var feedback = new FeedbackPlugin.Feedback();
var color = require("color");

require("nativescript-nodeify");
// const Tx = require('ethereumjs-tx');
const Eth = require('ethjs');
const EthAccount = require('ethjs-account');
//const EthQuery = require('ethjs-query');
const abi = require('ethjs-abi');
const sign = require('ethjs-signer').sign;
const unit = require('ethjs-unit');

function ThanksService() {

    if (ThanksService._instance) {
        throw new Error("Use ThanksService.getInstance() instead of new.");
    }

    this.thanks = fromObject({
      name : '',
      symbol : '',
      total : '',
      ethNode : 'http://46.105.124.156:4200',
      contract : '0xeEA8fEc6f7E477f4eAE131dedae8d70037002649',
      //'0x6280e219eA33be1E17803bC184c805687c108364',

      //'0xD7676aA3674367CE53874BBB8EA0E6d2b57F2D2c',
      //'0xB88DddA814fF5842F622ca596984a8D17483C091',
      contractABI : [{"constant":false,"inputs":[{"name":"target","type":"address"}],"name":"getAirdrop","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0x069f5bdd"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x06fdde03"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0x095ea7b3"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x18160ddd"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0x23b872dd"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x313ce567"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0x42966c68"},{"constant":true,"inputs":[],"name":"sellPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x4b750334"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x70a08231"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"mintedAmount","type":"uint256"}],"name":"mintToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0x79c65068"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0x79cc6790"},{"constant":true,"inputs":[],"name":"buyPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x8620410b"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"airdrops","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x8c86f0a7"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x8da5cb5b"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"airdropsfor","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x954093da"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function","signature":"0x95d89b41"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"foruser","type":"address"},{"name":"amount","type":"uint256"}],"name":"setAirdrop","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0x9b95f0a0"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"airdropDone","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function","signature":"0xa895fcf9"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0xa9059cbb"},{"constant":false,"inputs":[],"name":"getOldThanks","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0xb07884f5"},{"constant":true,"inputs":[{"name":"_of","type":"address"}],"name":"getPreviousBalance","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0xb18c4d86"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"frozenAccount","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function","signature":"0xb414d4b6"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"},{"name":"_extraData","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0xcae9ca51"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0xdd62ed3e"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"freeze","type":"bool"}],"name":"freezeAccount","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0xe724529c"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function","signature":"0xf2fde38b"},{"constant":true,"inputs":[{"name":"_of","type":"address"}],"name":"getBalance","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function","signature":"0xf8b2cb4f"},{"inputs":[{"name":"initialSupply","type":"uint256","value":"0"},{"name":"tokenName","type":"string","value":"Thanks"},{"name":"tokenSymbol","type":"string","value":"THX"}],"payable":false,"stateMutability":"nonpayable","type":"constructor","signature":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"target","type":"address"},{"indexed":false,"name":"frozen","type":"bool"}],"name":"FrozenFunds","type":"event","signature":"0x48335238b4855f35377ed80f164e8c6f3c366e54ac00b96a6402d4a9814a03a5"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"TransferTHX","type":"event","signature":"0xd6f70580f0c69fef8c06e37e1469bebd7b41c400d0960aa741e0f08cf4a2bf1e"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"ApprovalTHX","type":"event","signature":"0x28846ad06dfd5562f459aa9c88b2935056c688d1ea5f13f7412ddb609a5f2d9e"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"BurnTHX","type":"event","signature":"0x93795a01db5937597c89c0e119029e85f07023579170a0a628cd2f62dfc2e995"}],

      accounts : [],
      transactions : [],
    });

    // this.transaction = {
    //   amount:'',
    //   toname:'',
    //   toaddress:""
    // }

    this.randomPrivatekey = function() {
      var randHex = function(len) {
        var maxlen = 8,
            min = Math.pow(16,Math.min(len,maxlen)-1)
            max = Math.pow(16,Math.min(len,maxlen)) - 1,
            n   = Math.floor( Math.random() * (max-min+1) ) + min,
            r   = n.toString(16);
        while ( r.length < len ) {
           r = r + randHex( len - maxlen );
        }
        return r;
      };

      return '0x'+randHex(64);
    }

    this.getThanks = function() { return this.thanks };

    const eth = new Eth(new Eth.HttpProvider( this.thanks.ethNode ));
    const token = eth.contract(this.thanks.contractABI,'').at( this.thanks.contract );

    this.init = function() {
      //console.log(this.thanks);

      var nbtries =0;
      id = timerModule.setInterval(() => {
        eth.getLogs({
          fromBlock: '1',
          toBlock: 'latest',
          topics: ['0xd6f70580f0c69fef8c06e37e1469bebd7b41c400d0960aa741e0f08cf4a2bf1e'], //"_eventName":"TransferTHX"
        })
        .then((result) => {
          const contractABI = this.thanks.get('contractABI');
          const decoder = abi.logDecoder(contractABI)
          const events = decoder(result);
          const user = global.userService.getUser();

          const pk = userService.getPublickey();

          var myevents = events;
          // var myevents = events.filter(function(e){
          //   if (e.to==pk) return true;
          //   if (e.from==pk) return true;
          //   return false;
          // })

          myevents.reverse();

          // no new transaction...
          const tr = this.thanks.get('transactions');
          // no new transaction : do nothing
          if (tr.length==myevents.length)
            return;

          myevents.forEach(function(e){
            e.gotit=(e.from==pk)?true:false;
            e.sentit=(e.to==pk)?true:false;

            e.value = global.convertTHX(e.value);
          })

          this.thanks.set('transactions',myevents);

          const event = myevents[0];
          // transaction to me
          if (event.to.toLowerCase()==user.get('publickey').toLowerCase()) {
            global.userService.getBalance();
            feedback.show({
              title: "THX RECEIVED",
              titleColor: new color.Color("#F07000"),
              message: global.convertTHX(event.value)+" THX sent\nfrom "+event.from+"\nto "+event.to,
              messageColor: new color.Color("#FFFFFF"),
              duration: 5000,
              backgroundColor: new color.Color("#000000"),
            });

          }

          // transaction from me
          if (event.from.toLowerCase()==user.get('publickey').toLowerCase()) {
            global.userService.getBalance();
          }
        })
        .catch((error) => {
          nbtries++
          if (nbtries>42) {
            feedback.error({
              title: "NO BLOCKCHAIN",
              message: "BUS blockchain is not available\nVerify you have network, and try again"
            });
            nbtries=0;
          }
        });
      }, 5000);

      // get all accounts
      eth.accounts().then((result) => {
        this.thanks.set('accounts',result.sort());
      });

      token.name().then((n) => {
        this.thanks.set('name',n[0]);
      });

      token.symbol().then((n) => {
        this.thanks.set('symbol',n[0]);
      });

      token.totalSupply().then((totalSupply) => {
        this.thanks.set('total', global.convertTHX(totalSupply[0]) );
      });
      // unlock account



    };

    this.getOldThxBack = function() {
      // get previous THX back...
      if (userService.getPublickey()!='')
      eth.getTransactionCount( userService.getPublickey() ).then( (n) => {

        const contractABI = this.thanks.get('contractABI');
        const getOldThanks = contractABI.find(function(c){return c.name=='getOldThanks'});

        const ntx = sign({
          gasPrice: 0,
          gasLimit: 3000000,
          to: this.thanks.contract,
          data: abi.encodeMethod(getOldThanks, []),
          from: userService.getPublickey(),
          nonce: '0x' + n.toString(16)
        }, userService.getPrivatekey() );

        eth.sendRawTransaction(ntx, function(err, hash) {
          if (err) {
            console.log(err);
          } else {
            console.log('got old THX back !!!!');
            global.userService.getBalance();
          }

        });
      })
      .catch( (err) => {
        console.log(err);
      });
    }

    this.addressFromPrivate = function(private) {
      // 32 bytes = 32x2+'Ox' length...
      if (private.length!=66) return;
      const public = EthAccount.privateToPublic(private);
      return EthAccount.publicToAddress(public);
    }

    this.airdropThx = function(code) {
      const user = global.userService.getUser();
      const publickey = user.get('publickey');
      const contractABI = this.thanks.get('contractABI');

      const getAirdrop = contractABI.find(function(c){return c.name=='getAirdrop'});

      eth.getTransactionCount(publickey).then( (n) => {
        const ntx = sign({
          gasPrice: 0,
          gasLimit: 3000000,
          to: this.thanks.contract,
          data: abi.encodeMethod(getAirdrop, [code]),
          from: publickey,
          nonce: '0x' + n.toString(16)
        }, user.get('privatekey'));

        eth.sendRawTransaction(ntx, function(err, hash) {
          if (err) {
            //console.log(err);
            feedback.error({
              title: "NO AIRDROP",
              message: "No airdrop here, or you already have scanned it..."
            });
          } else { // OK !
            feedback.success({
              title: "AIRDROP OK",
              message: "You have received free Thanks from the BUS !!! "
            });
          }
        });
      }).catch(function(err){
        console.log(err);
      })
    }

    this.promiseThxBalanceOf = function (address) {
      return token.balanceOf(address);
    };

    this.sendThx = function (to,amount,offerid,message) {
      console.log("SEND "+amount+" THX to"+to);
      const user = global.userService.getUser();
      const publickey = user.get('publickey');
      const contractABI = this.thanks.get('contractABI');

      if (amount>user.get('thx'))
      {
        feedback.error({
        title: "NOT ENOUGH THX",
        message: "You can not send "+amount+" THX\nYou only have"+user.get('thx')+" THX..."
        });
        return;
      }

      eth.getTransactionCount(publickey).then( (n) => {

        const transfer = contractABI.find(function(c){return c.name=='transfer'});

        const ntx = sign({
          gasPrice: 0,
          gasLimit: 3000000,
          to: this.thanks.contract,
          data: abi.encodeMethod(transfer, [to,unit.toWei(amount, 'ether')]),
          from: publickey,
          nonce: '0x' + n.toString(16)
        }, user.get('privatekey'));

        eth.sendRawTransaction(ntx, function(err, hash) {
          if (err) {
            console.log(err);
            feedback.error({
              title: "THX NOT SENT",
              message: err
            });
          } else {

            // add to sold
            if (offerid)
            global.offers_firebase.doc(offerid).collection('sold').add({
              by_id: userService.getId(),
              date: firebase.firestore.FieldValue.serverTimestamp(),
              message: message?message:'',
            });

            // push notification to FMC receiver
            var touser = global.users._array.find( obj => {return obj.publickey === to } );
            if (touser.tokenFCM) {
              global.pushNotification(touser.tokenFCM,
              amount+' THX received',
              user.pseudo+' has just sent you '+amount+' THX\n'+message),
              {
                'page':'thanks/thanks-page',
                'type':'notification',
                'id':0,
              }
            }

            feedback.success({
              title: "TRANSACTION SENT",
              message: amount+" THX\nto :"+to
            });

            firebase.analytics.logEvent({
              key: "spend_virtual_currency",
              parameters: [ // optional
                {
                  key: "value",
                  value: amount
                }]
            });
          }

        });
      })
      .catch( (err) => {
        console.log(err);
      });
    };
}

ThanksService.getInstance = function () {
    return ThanksService._instance;
};

ThanksService._instance = new ThanksService();

module.exports = ThanksService;
