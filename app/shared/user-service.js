const Observable = require("tns-core-modules/data/observable").Observable;
const fromObject = require("tns-core-modules/data/observable").fromObject;
const appSettings = require("application-settings");
const geolocation = require("nativescript-geolocation");
const Accuracy =  require("tns-core-modules/ui/enums");
//const LocalNotifications = require("nativescript-local-notifications").LocalNotifications;
const dialogs = require("ui/dialogs");

const firebase = require("nativescript-plugin-firebase");
var firebaseApp =  require('nativescript-plugin-firebase/app');
// var firebaseAppAuth = firebaseApp.auth();

//const thanksService = require("./thanks-service")

var FeedbackPlugin = require("nativescript-feedback");
var feedback = new FeedbackPlugin.Feedback();
var color = require("color");

function UserService() {
    if (UserService._instance) {
        throw new Error("Use UserService.getInstance() instead of new.");
    }

    // appSettings.setString('id','QAjouHUkfFyr4TvXVr2i');
    // appSettings.setString('pseudo','on');
    // appSettings.setString('firstname','olivier');
    // appSettings.setString('lastname','nerot');
    // appSettings.setString('email','olivier@thebus.io');
    // appSettings.setString('presentation','test...');
    // appSettings.setString('privatekey','');
    // appSettings.setString('publickey','');
    // appSettings.setBoolean('isRegistered',false);

    this.user = fromObject({
        id : appSettings.getString('id',''),
        pseudo : appSettings.getString('pseudo',''),
        firstname : appSettings.getString('firstname',''),
        lastname : appSettings.getString('lastname',''),
        email : appSettings.getString('email',''),
        presentation : appSettings.getString('presentation',''),
        thx : appSettings.getNumber('thx'),
        image: appSettings.getString('image','https://firebasestorage.googleapis.com/v0/b/mybus-97f0f.appspot.com/o/users%2Fprofileb.png?alt=media&token=8c4ee126-e9bd-46cd-beb1-a7693f9ad730'),
        isRegistered: appSettings.getBoolean('isRegistered',false),
        publickey : '', //appSettings.getNumber('publickey'), //void to be initialized afterward to setup thx
        privatekey : appSettings.getString('privatekey',''),
        notifications : JSON.parse( appSettings.getString('notifications','[]') ),
        favorites: JSON.parse( appSettings.getString('favorites','[]') ),
    });

    this.options = fromObject({
        followMe : appSettings.getBoolean('followMe',false),
        codeRequired : appSettings.getBoolean('codeRequired',false),
        enableNotification : appSettings.getBoolean('enableNotification',false),
        code : appSettings.getString('code',''),
        anonymous : appSettings.getBoolean('anonymous',false),
        visible: appSettings.getBoolean('visible',true),
        autoLogout: appSettings.getBoolean('autoLogout',false),
        lockedpk: appSettings.getBoolean('lockedpk',false),
    })

    this.getUser = function() { return this.user};
    this.getOptions = function() { return this.options};

    this.getName = function() {
      if (this.options.anonymous) return this.user.pseudo;
      return this.user.pseudo+" ("+this.user.firstname+" "+this.user.lastname+")";
    }

    this.getId = function() {
      return this.user.get('id');
    }
    this.setId = function(id) {
      this.user.set('id',id);
    }

    this.getImage = function() {
      return this.user.get('image');
    }
    this.setImage = function(url) {
      this.user.set('image',url);
    }

    this.getPseudo = function() {
      return this.user.get('pseudo');
    }
    this.setPseudo = function(data) {
      this.user.set('pseudo',data);
    }

    this.getFirstname = function() {
      return this.user.get('firstname');
    }
    this.setFirstname = function(data) {
      this.user.set('firstname',data);
    }

    this.getLastname = function() {
      return this.user.get('lastname');
    }
    this.setLastname = function(data) {
      this.user.set('lastname',data);
    }

    // this.getFullname = function() {
    //   return this.user.get('firstname') + " " + this.user.get('lastname');
    // }

    this.getEmail = function() {
      return this.user.get('email');
    }
    this.setEmail = function(data) {
      this.user.set('email',data);
    }

    this.getPresentation = function() {
      return this.user.get('presentation');
    }
    this.setPresentation = function(data) {
      this.user.set('presentation',data);
    }

    this.getVisible = function() {
      return this.options.get('visible');
    }
    this.setVisible = function(data) {
      this.options.set('visible',data);
    }

    this.getAnonymous = function() {
      return this.options.get('anonymous');
    }
    this.setAnonymous = function(data) {
      this.options.set('anonymous',data);
    }

    this.getPublickey = function() {
      return this.user.get('publickey');
    }
    this.setPublickey = function(data) {
      this.user.set('publickey',data);
    }

    this.getPrivatekey = function() {
      return this.user.get('privatekey');
    }
    this.setPrivatekey = function(data) {
      this.user.set('privatekey',data);
    }

    this.getLockedPk = function() {
      return this.options.get('lockedpk');
    }
    this.setLockedPk = function(data) {
      this.options.set('lockedpk',data);
    }

    this.getNotifications = function() {
      return this.user.get('notifications');
    }
    this.setNotifications = function(data) {
      this.user.set('notifications',data);
    }

    this.getCode = function() {
      return this.options.get('code');
    }

    this.isFavorite = function(uid) {
      var f = this.user.get('favorites');
      return (f.find(function(id){return (id==uid)})!=undefined)
      //return false;
    }

    // init user : update thx account by setting publickey...
    this.init = function() {
        this.user.set('publickey', appSettings.getString('publickey','') );
        if (this.options.followMe==true)
            this.startLocation();
    };

    // this.addTransaction = function (hash) {
    //   // var t = this.user.get('transactions');
    //   // t.push(hash);
    //   // this.user.set('transactions',t);
    // }

    this.isLogged = function() {
      return (appSettings.getBoolean('islogged'));
    }

    this.getBalance = function() {
      global.thanksService.promiseThxBalanceOf(this.user.get('publickey')).then(
        (thx) => {
          this.user.set('thx', global.convertTHX(thx[0]));
        }
      )
    }

    this.sendMessage = function(message) {
      console.log("Title: " + message.title);
      console.log("Body: " + message.body);
      console.log(message.data);
      feedback.show({
        title: message.title,
        titleColor: new color.Color("#F07000"),
        message: message.body,
        messageColor: new color.Color("#FFFFFF"),
        duration: 5000,
        backgroundColor: new color.Color("#606060"),
      });

      var n = this.getNotifications();
      message.id = n.length;
      n.push({
        title:message.title,
        body:message.body
      });
      this.setNotifications(n);
    }


    var watchIds = [];
    this.startLocation = function() {
      console.log("START LOCATION !!!!")
        try {
            watchIds.push(geolocation.watchLocation(
                function (loc) {
                  console.log("GET LOC :");
                  console.log(loc);
                    if (loc) {
                      if (global.users_firebase)
                      global.users_firebase.doc(global.userService.getId()).update({
                        location: firebase.firestore.GeoPoint(loc.latitude, loc.longitude),
                      });
                      global.offers.forEach(function(offer){
                        if (offer.location&&((offer.location.latitude!=0)&&(offer.location.longitude!=0)))
                          offer.set('dist',parseInt( geolocation.distance(loc,offer.location) ));
                        else
                          offer.set('dist', undefined);

                        console.log(offer.get('dist'));
                      });
                      global.users.forEach(function(user){
                        if (user.location&&((user.location.latitude!=0)&&(user.location.longitude!=0)))
                          user.set('dist',parseInt( geolocation.distance(loc,user.location) ));
                        else
                          user.set('dist',undefined);

                      });
                      global.places.forEach(function(place){
                        if (place.location&&((place.location.latitude!=0)&&(place.location.longitude!=0)))
                          place.set('dist',parseInt( geolocation.distance(loc,place.location) ));
                        else
                          place.set('dist',undefined);

                      });

                      // sort places by dist
                      global.places.sort(function(a, b) {
                        a=a.dist?a.dist:1E18;
                        b=b.dist?b.dist:1E18;
                        return (a==b)?0:(a>b)?1:-1;
                      });

                      // sort offers by dist
                      global.offers.sort(function(a, b) {
                        var da=a.dist?a.dist:1E18;
                        var db=b.dist?b.dist:1E18;
                        if (!a.active) da = 2E18;
                        if (!b.active) db = 2E18;
                        return (da==db)?0:(da>db)?1:-1;
                      });
                    }
                },
                function (e) {
                    console.log("Error: " + e.message);
                },
                {
                    desiredAccuracy: Accuracy.high,
                    updateDistance: 10,
                    updateTime: 10000,
                    minimumUpdateTime: 1000
                }));
        } catch (ex) {
            console.log("Error: " + ex.message);
        }
    }

    this.stopLocation =  function() {
        let watchId = watchIds.pop();
        while (watchId != null) {
            geolocation.clearWatch(watchId);
            watchId = watchIds.pop();
        }
    }

    // user profile changed...
    this.user.addEventListener(Observable.propertyChangeEvent, (args) => {
        //args is of type PropertyChangeData
        console.log("propertyChangeEvent [eventName]: ", args.eventName);
        console.log("propertyChangeEvent [propertyName]: ", args.propertyName);
        console.log("propertyChangeEvent [value]: ", args.value);
        console.log("propertyChangeEvent [oldValue]: ", args.oldValue);

        if (args.value==undefined) return;

        switch(args.propertyName) {
          case 'thx':
            appSettings.setNumber('thx',parseInt(args.value));
            break;
          case 'publickey':
          if (global.users_firebase)
            global.users_firebase.doc(this.getId()).update({
              publickey: args.value,
            })
            appSettings.setString('publickey',args.value);

            global.thanksService.getOldThxBack();

            this.getBalance();
            break;
          case 'privatekey':
            // save privatekey
            appSettings.setString('privatekey',args.value);
            // set user publickey/address
            const address = global.thanksService.addressFromPrivate(args.value);
            if (address==undefined)
              this.user.set('publickey','');
            else
              this.user.set('publickey',address.toLowerCase());
            // update blockchain
            break;
          // case 'transactions':
          //   appSettings.setString('transactions',JSON.stringify(args.value))
          //   break;
          case 'isRegistered':
            appSettings.setBoolean('isRegistered',args.value);
            break;
          case 'notifications':
            appSettings.setString('notifications',JSON.stringify(args.value));
            break;
          default :
            appSettings.setString(args.propertyName,args.value);
        }
    });

    this.options.addEventListener(Observable.propertyChangeEvent, (args) => {
      if (args.value==undefined) return;

      console.log("propertyChangeEvent [eventName]: ", args.eventName);
      console.log("propertyChangeEvent [propertyName]: ", args.propertyName);
      console.log("propertyChangeEvent [value]: ", args.value);
      console.log("propertyChangeEvent [oldValue]: ", args.oldValue);

      switch(args.propertyName) {
        case 'followMe':
          appSettings.setBoolean('followMe',args.value);
          if( args.value==true ) {
            geolocation.enableLocationRequest();
            this.startLocation();
          }
          else {
            this.stopLocation();
          }
          break;
        case 'codeRequired':
          appSettings.setBoolean('codeRequired',args.value);
          break;
        case 'enableNotification':
          appSettings.setBoolean('enableNotification',args.value);
          if( args.value==true ) {
            enableNotification();
          }
          break;
        case 'anonymous':
          appSettings.setBoolean('anonymous',args.value);
          global.users_firebase.doc(this.getId()).update({
            anonymous: args.value,
          })
          break;
        case 'visible':
          appSettings.setBoolean('visible',args.value);
          global.users_firebase.doc(this.getId()).update({
            visible: args.value,
          })
          break;
        case 'autoLogout':
          appSettings.setBoolean('autoLogout',args.value);
          break;
        default :
          appSettings.setBoolean(args.propertyName,args.value);
      }
    });
}

function enableNotification() {

  firebase.analytics.logEvent({
    key: "enable_notification"
  })

  firebase.addOnMessageReceivedCallback(function(message){
    const us = UserService.getInstance();
    us.sendMessage(message);
  })


  firebase.getCurrentPushToken().then(function(token) {
    const us = UserService.getInstance();
    console.log("token="+token);
    global.users_firebase.doc(us.getId()).update({
      tokenFCM: token,
    })
  });

  firebase.subscribeToTopic("pushok");

  // firebase.analytics.setUserProperty({
  //   key: "nb_offers",
  //   value: 12
  // })
  //
  // firebase.analytics.setUserProperty({
  //   key: "thx_sent",
  //   value: 42
  // })

//
// public onChangePassword() {
//     firebase.changePassword({
//         email: this.email,
//         oldPassword: 'myOldPassword',
//         newPassword: 'myNewPassword'
//     }).then(() => {
//         // called when password change was successful
//     }).catch(err => {
//         console.log(err);
//         dialogs.alert(err);
//     })
// }



}

UserService.getInstance = function () {
    return UserService._instance;
};

UserService._instance = new UserService();

module.exports = UserService;
