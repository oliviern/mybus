const observableModule = require("tns-core-modules/data/observable");
const application = require("tns-core-modules/application");
const platformModule = require("tns-core-modules/platform");

function CameraViewModel() {
    //SelectedPageService.getInstance().updateSelectedPage("Camera");

    const viewModel = observableModule.fromObject({
      cameraHeight : platformModule.screen.mainScreen.heightDIPs,
    });

    return viewModel;
}

module.exports = CameraViewModel;
