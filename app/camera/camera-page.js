const observable = require('tns-core-modules/data/observable');
const pages = require('tns-core-modules/ui/page');
const CameraViewModel = require('./camera-view-model');
//const CameraPlus =require('@nstudio/nativescript-camera-plus');
const CameraPlus = require('@nstudio/nativescript-camera-plus').CameraPlus;

var cam = undefined;

// Event handler for Page 'loaded' event attached in main-page.xml
function pageLoaded(args) {
  // Get the event sender
  let page = args.object;
  page.bindingContext = new CameraViewModel();

  let navigationBar = page.actionBar.ios;
  if (navigationBar)
    navigationBar.topItem.hidesBackButton = true;
}

function camLoaded(args) {
  console.log(`cam loaded event`);

  cam = args.object;

  cam.on(CameraPlus.errorEvent, args => {
    console.log('*** CameraPlus errorEvent ***', args);
  });

  cam.on(CameraPlus.toggleCameraEvent, (args) => {
    console.log(`toggleCameraEvent listener on main-view-model.ts  ${args}`);
  });

  cam.on(CameraPlus.photoCapturedEvent, (args) => {
    console.log(`photoCapturedEvent listener on main-view-model.ts  ${args}`);
    console.log((args).data);
    fromAsset((args).data).then(res => {
      const testImg = topmost().getViewById('testImagePickResult') ;
      testImg.src = res;
    });
  });

  cam.on(CameraPlus.imagesSelectedEvent, (args) => {
    console.log(`imagesSelectedEvent listener on main-view-model.ts ${args}`);
  });

  // cam.on(CameraPlus.videoRecordingReadyEvent, (args) => {
  //   console.log(`videoRecordingReadyEvent listener fired`, args.data);
  // });
  //
  // cam.on(CameraPlus.videoRecordingStartedEvent, (args) => {
  //   console.log(`videoRecordingStartedEvent listener fired`, args.data);
  // });
  //
  // cam.on(CameraPlus.videoRecordingFinishedEvent, (args) => {
  //   console.log(`videoRecordingFinishedEvent listener fired`, args.data);
  // });

}

function imagesSelectedBinding(args) {
  console.log(args);
}

// function recordDemoVideo() {
//   try {
//     console.log(`*** start recording ***`);
//     cam.record();
//   } catch (err) {
//     console.log(err);
//   }
// }
//
// function stopRecordingDemoVideo() {
//   try {
//     console.log(`*** stop recording ***`);
//     cam.stop();
//     console.log(`*** after cam.stop() ***`);
//   } catch (err) {
//     console.log(err);
//   }
// }

function toggleFlashOnCam() {
  cam.toggleFlash();
}

function toggleShowingFlashIcon() {
  console.log(`showFlashIcon = ${cam.showFlashIcon}`);
  cam.showFlashIcon = !cam.showFlashIcon;
}

function toggleTheCamera() {
  cam.toggleCamera();
}

function openCamPlusLibrary() {
  cam.chooseFromLibrary().then(
    (images) => {
      console.log('Images selected from library total:', images.length);
      for (let source of images) {
        console.log(`source = ${source}`);
      }
      const testImg = topmost().getViewById('testImagePickResult') ;
      const firstImg = images[0];
      console.log(firstImg);
      fromAsset(firstImg)
        .then(res => {
          const testImg = topmost().getViewById('testImagePickResult') ;
          testImg.src = res;
        })
        .catch(err => {
          console.log(err);
        });
    },
    err => {
      console.log('Error -> ' + err.message);
    }
  );
}

function takePicFromCam() {
  cam.requestCameraPermissions().then(() => {
    if (!cam) {
      cam = new CameraPlus();
    }
    cam.takePicture({ saveToGallery: true });
  });
}

exports.pageLoaded = pageLoaded;
exports.camLoaded = camLoaded;
exports.imagesSelectedBinding = imagesSelectedBinding;
exports.toggleFlashOnCam = toggleFlashOnCam;
exports.toggleShowingFlashIcon = toggleShowingFlashIcon;
exports.toggleTheCamera = toggleTheCamera;
exports.openCamPlusLibrary = openCamPlusLibrary;
exports.takePicFromCam = takePicFromCam;
