const app = require("tns-core-modules/application");
//const OfferLocationModel = require("./offer-location-model");
const firebase = require("nativescript-plugin-firebase");

var context;

function onNavigatingTo(args) {
    const page = args.object;
    context = args.context;
    //page.bindingContext = new OfferLocationModel();
}

function onMapReady(args) {
  var nativeMapView = args.ios ? args.ios : args.android;

  args.map.setOnMapClickListener( function(point) {
    console.log("Map clicked at latitude: " + point.lat + ", longitude: " + point.lng);
    args.map.removeMarkers();

    args.map.addMarkers([
      {
        lat: point.lat,
        lng: point.lng,
      }]
    );

    context.location = firebase.firestore.GeoPoint(point.lat,point.lng);
  });
  
  if ((context.location.latitude==0)&&(context.location.longitude==0))
    return;

  args.map.addMarkers([
    {
      lat: context.location.latitude,
      lng: context.location.longitude,
    }]
  );

  args.map.setCenter(
    {
      lat: context.location.latitude, // mandatory
      lng: context.location.longitude, // mandatory
      animated: false // default true
    }
  )

}

exports.onNavigatingTo = onNavigatingTo;
exports.onMapReady = onMapReady;
