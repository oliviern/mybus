const app = require("tns-core-modules/application");
const frameModule = require("tns-core-modules/ui/frame");
const platformModule = require("platform");
const http = require("http");
const Color = require("tns-core-modules/color").Color;
const AllViewModel = require("./all-view-model");
const firebase = require("nativescript-plugin-firebase");
const UserService = require("../shared/user-service");

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = new AllViewModel();
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

// function onAddPlaceTap(args) {
//   // alert('add place');
// }

function onItemLoading(args) {
    // hack to get around issue with RadListView ios background colors: https://github.com/telerik/nativescript-ui-feedback/issues/196
    if (platformModule.isIOS) {
        var newcolor = new Color("#303030");
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

function onOpenPlace(args) {
  const item = args.object.items.getItem(args.index);
  args.object.page.frame.navigate({
    moduleName: "./pageplace/pageplace-page",
    context: item,
  });
}

function onOpenUser(args) {
  const item = args.object.items.getItem(args.index);
  args.object.page.frame.navigate({
    moduleName: "./pageuser/pageuser-page",
    context: item,
  });
}

function onOpenOffer(args) {
  const item = args.object.items.getItem(args.index);
  args.object.page.frame.navigate({
    moduleName: "./offer/offer-page",
    context: item,
  });
}

var mapbox = require("nativescript-mapbox");

function onMapReady(args) {
  var nativeMapView = args.ios ? args.ios : args.android;

  if (global.offers)
  global.offers.forEach(function(offer){

    if ((offer.location.latitude==0)&&(offer.location.longitude==0))
      return;

    args.map.addMarkers([
      {
        id: offer.id,
        lat: offer.location.latitude,
        lng: offer.location.longitude,
        title: offer.title,
        subtitle: offer.content,
        iconPath: 'images/BUSmini_icon.png',
        selected: false, // makes the callout show immediately when the marker is added (note: only 1 marker can be selected at a time)
        onCalloutTap: function(marker){
          const offer = global.offers._array.find( obj => {return obj.id === marker.id } );
          if (offer)
            frameModule.topmost().navigate({
              moduleName: "./offer/offer-page",
              context: {
                id: offer.id,
                owner_id: offer.owner_id
              },
              animated: true,
              transition: {
                name: "slideLeft",
                duration: 380,
                curve: "easeIn"
              }
            });
          }
        }]
      );
    });

  if (global.places)
  global.places.forEach(function(place){
    if ((place.location.latitude==0)&&(place.location.longitude==0))
      return;

      args.map.addMarkers([
        {
          id: place.id,
          lat: place.location.latitude,
          lng: place.location.longitude,
          title: place.title,
          subtitle: place.content,
          iconPath: 'images/BUS_icon.png',
          selected: false, // makes the callout show immediately when the marker is added (note: only 1 marker can be selected at a time)
          onCalloutTap: function(marker){
            const place = global.places._array.find( obj => {return obj.id === marker.id } );
            if (place)
            frameModule.topmost().navigate({
              moduleName: "./pageplace/pageplace-page",
              context: place,
              animated: true,
              transition: {
                name: "slideLeft",
                duration: 380,
                curve: "easeIn"
              }
            });
          }
        }]
      );
    });

    // args.map.setOnScrollListener((point) => {
    //   console.log("Map scrolled to latitude: " + point.lat + ", longitude: " + point.lng);
    // });

    args.map.setTilt(
        {
          tilt: 60, // default 30 (degrees angle)
          duration: 4000 // default 5000 (milliseconds)
        }
    );

  // args.map.getUserLocation().then(
  //     function(userLocation) {
  //       console.log("USER LOCATION");
  //       console.log("Current user location: " +  userLocation.location.lat + ", " + userLocation.location.lng);
  //       console.log("Current user speed: " +  userLocation.speed);
  //
  //       console.log(userLocation);
  //       console.log(global.users_firebase);
  //       console.log(global.userService.getId());
  //       if (global.users_firebase)
  //       global.users_firebase.doc(global.userService.getId()).update({
  //         location: firebase.firestore.GeoPoint(userLocation.location.lat, userLocation.location.lng),
  //       })
  //     }
  // );

  args.map.trackUser({
    mode: "FOLLOW", // "NONE" | "FOLLOW" | "FOLLOW_WITH_HEADING" | "FOLLOW_WITH_COURSE"
    animated: true
  });

  // args.map.setOnMapClickListener( function(point) {
  //   console.log("Map clicked at latitude: " + point.lat + ", longitude: " + point.lng);
  // });

}

// function pullRefreshOffers(e) {
//     global.getItems().then(function(result) {
//       global.items = result;
//       e.object.items = global.items;
//       e.object.notifyPullToRefreshFinished();
//     }, function(error) {
//         console.error(JSON.stringify(error));
//         e.object.notifyPullToRefreshFinished();
//     });
// };

// function pullRefreshPlaces(e) {
//     global.getPlaces().then(function(result) {
//       global.places = result;
//       e.object.places = global.places;
//       e.object.notifyPullToRefreshFinished();
//     }, function(error) {
//         console.error(JSON.stringify(error));
//         e.object.notifyPullToRefreshFinished();
//     });
// };

function onFilter(args) {
  args.object.page.frame.navigate({
    moduleName: "./all-filter/all-filter-page",
    animated: true,
    //context : item,
    transition: {
      name: "slideTop",
      duration: 380,
      curve: "easeIn"
    }
  });

  // get all balances...
  // global.users._array.forEach(function(u){
  //   global.thanksService.promiseThxBalanceOf(u.publickey).then(
  //     (thx) => {
  //       console.log(u.pseudo+'('+u.publickey+') : '+convertTHX(thx[0]) );
  //     }
  //   )
  // })

}

exports.onMapReady = onMapReady;
//exports.pullRefreshOffers = pullRefreshOffers;
//exports.pullRefreshPlaces = pullRefreshPlaces;
exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
//exports.onAddPlaceTap = onAddPlaceTap;
exports.onItemLoading = onItemLoading;
exports.onOpenPlace = onOpenPlace;
exports.onOpenUser = onOpenUser;
exports.onOpenOffer = onOpenOffer;
exports.onFilter = onFilter;
