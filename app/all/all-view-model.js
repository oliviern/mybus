const observableModule = require("tns-core-modules/data/observable");
const SelectedPageService = require("../shared/selected-page-service");

function AllViewModel() {
    SelectedPageService.getInstance().updateSelectedPage("All");

    const viewModel = observableModule.fromObject({
      users: global.users,
      offers: global.offers,
      places: global.places
    });

    return viewModel;
}

module.exports = AllViewModel;
