const app = require("tns-core-modules/application");
const platformModule = require("platform");
const frameModule = require("tns-core-modules/ui/frame");
const utilsModule = require("tns-core-modules/utils/utils");
//const ItemViewModel = require("./item-view-model");

function onNavigatingTo(args) {
    const page = args.object;
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

function onTapFb(args) {
  utilsModule.openUrl("https://www.facebook.com/bottomupsociety/");

  // global.pushNotification('dU6-UD8APZ0:APA91bHzGCDRUV_2ddqzVaWYuAMrQCM1DJHkF8rwyo51E03JThjZArlDLinUkSnBfr9zwxahXxk7DyICH8nT2NDx10KdcxH2XoPIb-J1OPGj6BG_ldhGfz4bSeIl292t3YYSJyPqRnMh',
  // 'hello',
  // 'ca marche')
}

function onTapTw(args) {
  utilsModule.openUrl("https://twitter.com/BottomUpSociety");
}

function onTapWeb(args) {
  utilsModule.openUrl("http://thebus.io");
}

function onTapDiscord(args) {
  utilsModule.openUrl("https://discord.gg/f4uuTg");
}

exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
exports.onTapFb = onTapFb;
exports.onTapTw = onTapTw;
exports.onTapWeb = onTapWeb;
exports.onTapDiscord = onTapDiscord;
